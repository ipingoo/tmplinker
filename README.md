Since 2.0.0 version *tmplinker* is the middleware for the [express](https://www.npmjs.com/package/express).

```javascript
var express = require('express');
var router = express.Router();
var tmplinker = require('tmplinker');
tmplinker.plugin(router, {
  secureDir: 'secure',
  secret: 'ewjr98456y4h5th',
  algorithm: 'rc4',
  expirationTime: '6h', // Use 'none' for unlimited links
  route: '/files'
});
```

## Installation

```
$ npm install tmplinker
```

## API

Tmplinker's api have 2 methods `plugin()` and `createLink()`.

### plugin()
`plugin(router, {params})`

`router` (required) - express router, [function]

*params:*

* `secureDir` (required) - your secure directory in root folder, [string]
* `secret` (optional) - your password, default: Just A Secret, [string]
* `algorithm` (optional) - algorithm for encryption link, default: aes192, [string]
* `expirationTime` (optional) - time life link. Use '*none*' for unlimited links, default: 3h, [integer] or [string]
* `route` (optional) - route for download like a *http://localhost:3000/download/:hash*, default: /download, [string]

##### expirationTime
You can use Integer value for seconds like a *3600* or *3600s*. And pattern like a *45i (45 minutes)*, *3h (3 hours)*, *1d (1 day)*, etc.

```
s: 1,        // seconds
i: 60,       // minute
h: 3600,     // hour
d: 43200,    // day
w: 302400,   // week
m: 9072000,  // month (30 days)
y: 108864000 // year
```


### createLink()
`createLink(filename, callback)`

`filename` (required) - name of the file in secure directory, [string]

#### Example

```javascript
router.post('/createlink', function(req, res, next) {
  tmplinker.createLink(req.body.filename, (err, link) => {
    if(err)
      return next();
    res.status(200).send(`Your personal link: ${link}`);
  });
});
```

---

##### For additional information
[Crypto](https://nodejs.org/api/crypto.html "Node.js Documentation")

[Full example](https://bitbucket.org/ipingoo/tmplinker "See full example")

---

##### Node.js defaultCipherList
[ 'CAST-cbc',
  'aes-128-cbc',
  'aes-128-cbc-hmac-sha1',
  'aes-128-cfb',
  'aes-128-cfb1',
  'aes-128-cfb8',
  'aes-128-ctr',
  'aes-128-ecb',
  'aes-128-gcm',
  'aes-128-ofb',
  'aes-128-xts',
  'aes-192-cbc',
  'aes-192-cfb',
  'aes-192-cfb1',
  'aes-192-cfb8',
  'aes-192-ctr',
  'aes-192-ecb',
  'aes-192-gcm',
  'aes-192-ofb',
  'aes-256-cbc',
  'aes-256-cbc-hmac-sha1',
  'aes-256-cfb',
  'aes-256-cfb1',
  'aes-256-cfb8',
  'aes-256-ctr',
  'aes-256-ecb',
  'aes-256-gcm',
  'aes-256-ofb',
  'aes-256-xts',
  'aes128',
  'aes192',
  'aes256',
  'bf',
  'bf-cbc',
  'bf-cfb',
  'bf-ecb',
  'bf-ofb',
  'blowfish',
  'camellia-128-cbc',
  'camellia-128-cfb',
  'camellia-128-cfb1',
  'camellia-128-cfb8',
  'camellia-128-ecb',
  'camellia-128-ofb',
  'camellia-192-cbc',
  'camellia-192-cfb',
  'camellia-192-cfb1',
  'camellia-192-cfb8',
  'camellia-192-ecb',
  'camellia-192-ofb',
  'camellia-256-cbc',
  'camellia-256-cfb',
  'camellia-256-cfb1',
  'camellia-256-cfb8',
  'camellia-256-ecb',
  'camellia-256-ofb',
  'camellia128',
  'camellia192',
  'camellia256',
  'cast',
  'cast-cbc',
  'cast5-cbc',
  'cast5-cfb',
  'cast5-ecb',
  'cast5-ofb',
  'des',
  'des-cbc',
  'des-cfb',
  'des-cfb1',
  'des-cfb8',
  'des-ecb',
  'des-ede',
  'des-ede-cbc',
  'des-ede-cfb',
  'des-ede-ofb',
  'des-ede3',
  'des-ede3-cbc',
  'des-ede3-cfb',
  'des-ede3-cfb1',
  'des-ede3-cfb8',
  'des-ede3-ofb',
  'des-ofb',
  'des3',
  'desx',
  'desx-cbc',
  'id-aes128-GCM',
  'id-aes192-GCM',
  'id-aes256-GCM',
  'idea',
  'idea-cbc',
  'idea-cfb',
  'idea-ecb',
  'idea-ofb',
  'rc2',
  'rc2-40-cbc',
  'rc2-64-cbc',
  'rc2-cbc',
  'rc2-cfb',
  'rc2-ecb',
  'rc2-ofb',
  'rc4',
  'rc4-40',
  'rc4-hmac-md5',
  'seed',
  'seed-cbc',
  'seed-cfb',
  'seed-ecb',
  'seed-ofb' ]
