var expect = require('chai').expect
var linker = require('./index')
var validator = require('./validator')
var link   = 'http://somelink.com/file.pdf'

describe("Tests for validate link", () => {
  it("validate with 'none' should return link", done => {
    var res = validator.validateLink({body: link, expirationTime: 'none', createdAt: 1472297439})
    expect(res).to.equal(link)
    done()
  })

  it("validate with no expired link should return link", done => {
    var res = validator.validateLink({body: link, expirationTime: 1800, createdAt: Date.now() / 1000 | 0})
    expect(res).to.equal(link)
    done()
  })

  it("validate expired link should return error", done => {
    var res = validator.validateLink({body: link, expirationTime: 3600, createdAt: 1472293439})
    expect(res).to.be.an('error')
    done()
  })
})

describe("Tests for validate expirationTime", () => {
  var postfix = ['s', 'i', 'h', 'd', 'w', 'm', 'y']
  it("validate with postfix should return number", done => {
    postfix.map((i) => {
      var res = validator.validateExpirationTime(`4${i}`)
      expect(res).to.be.a('number')
      return
    })
    done()
  })

  var wrongPostfix = ['g', 'v', 'c', 'a', 'q', 'z', 'l']
  it("validate with wrong postfix should return error", done => {
    wrongPostfix.map((i) => {
      var res = validator.validateExpirationTime(`4${i}`)
      expect(res).to.be.an('error')
      return
    })
    done()
  })

  it("validate without postfix should return number", done => {
    var t = 2450
    var res = validator.validateExpirationTime(t)
    expect(res).to.equal(t)
    done()
  })

  it("validate with wrong expirationTime should return error", done => {
    var res = validator.validateExpirationTime('rw8')
    expect(res).to.be.an('error')
    done()
  })
})

// describe("Tests for encrypt", () => {
//   it("make() with params (link) should return encrypted string", done => {
//     linker.make({link: link}, (err, data) => {
//       expect(err).to.be.null
//       expect(data).to.be.a('string')
//       done()
//     })
//   })
//
//   it("make() with no params should return error", done => {
//     linker.make({}, (err, data) => {
//       expect(err).to.be.an('error')
//       expect(data).to.be.null
//       done()
//     })
//   })
//
//   it("make() with params not like link should return error", done => {
//     linker.make({expirationTime: 1800}, (err, data) => {
//       expect(err).to.be.an('error')
//       expect(data).to.be.null
//       done()
//     })
//   })
//
//   it("make() with wrong expirationTime (like the -3600) should return error", done => {
//     linker.make({link: link, expirationTime: -3600}, (err, data) => {
//       expect(err).to.be.an('error')
//       expect(data).to.be.null
//       done()
//     })
//   })
//
//   it("make() with wrong expirationTime (like the tgerg) should return error", done => {
//     linker.make({link: link, expirationTime: 'tgerg'}, (err, data) => {
//       expect(err).to.be.an('error')
//       expect(data).to.be.null
//       done()
//     })
//   })
//
//   it("make() with wrong algorithm should return error", done => {
//     linker.make({link: link, algorithm: 'rueth4'}, (err, data) => {
//       expect(err).to.be.an('error')
//       expect(data).to.be.null
//       done()
//     })
//   })
// })
//
// describe("Tests for decrypt", (suite) => {
//   it("parse() with params (link) should return link", done => {
//     linker.make({link: link}, (err, data) => {
//       linker.parse({link: data}, (decryptedErr, decryptedLink) => {
//         expect(decryptedErr).to.be.null
//         expect(decryptedLink).to.equal(link)
//         done()
//       })
//     })
//   })
//
//   it("parse() with another password should return error", done => {
//     linker.make({link: link, password: 'somepass1'}, (err, data) => {
//       linker.parse({link: data}, (decryptedErr, decryptedLink) => {
//         expect(decryptedErr).to.be.an('error')
//         expect(decryptedLink).to.be.null
//         done()
//       })
//     })
//   })
//
//   it("parse() with another algorithm should return error", done => {
//     linker.make({link: link, algorithm: 'aes-256-ctr'}, (err, data) => {
//       linker.parse({link: data}, (decryptedErr, decryptedLink) => {
//         expect(decryptedErr).to.be.an('error')
//         expect(decryptedLink).to.be.null
//         done()
//       })
//     })
//   })
//
//   it("parse() with expired link should return error", done => {
//     linker.make({link: link, expirationTime: 1}, (err, data) => {
//       setTimeout(() => {
//         linker.parse({link: data}, (decryptedErr, decryptedLink) => {
//           expect(decryptedErr).to.be.an('error')
//           expect(decryptedLink).to.be.null
//           done()
//         })
//       }, 2000)
//     })
//   })
// })
