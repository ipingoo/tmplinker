'use strict'

var crypto = require('crypto')
var path = require('path')
var validator = require('./validator')
var preset = {
  secret: 'Just A Secret',
  algorithm: 'aes192',
  expirationTime: '3h',
  route: '/download'
}

exports.plugin = (router, data) => {
  if(typeof router !== 'function')
    throw new Error('Router is required')
  else if(typeof data.secureDir === 'undefined')
    throw new Error('Secure directory is required')

  preset.secureDir = data.secureDir
  preset.secret = typeof data.secret === 'undefined' ? preset.secret : data.secret
  preset.algorithm = typeof data.algorithm === 'undefined' ? preset.algorithm : data.algorithm
  preset.expirationTime = typeof data.expirationTime === 'undefined'
                        ? validator.validateExpirationTime(preset.expirationTime)
                        : validator.validateExpirationTime(data.expirationTime)
  preset.route = typeof data.route === 'undefined' ? preset.route : data.route

  if(preset.expirationTime instanceof Error)
    throw preset.expirationTime

  router.get(`${preset.route}/:link`, (req, res, next) => {
    parseLink(req.params.link, (err, filename) => {
      if(err || typeof filename === 'undefined')
        res.status(400).end('Bad Request')
      else
        res.download(path.join(__dirname, '/../../', preset.secureDir, filename))
    })
  })
}

exports.createLink = (link, callback) => {
  if(typeof link === 'undefined') {
    callback(new Error('Link is required'), null)
    return
  } else {
    let obj = {
      body: link,
      expirationTime: preset.expirationTime,
      createdAt: Date.now() / 1000 | 0
    }
    try {
      let cipher = crypto.createCipher(preset.algorithm, preset.secret)
      var crypted = cipher.update(JSON.stringify(obj), 'utf8', 'hex')
        + cipher.final('hex')
    } catch(err) {
      callback(err, null)
      return
    }
    callback(null, `${preset.route}/${crypted}`)
  }
}

var parseLink = (link, callback) => {
  if(typeof link === 'undefined') {
    callback(new Error('Link is required'), null)
  } else {
    try {
      let decipher = crypto.createDecipher(preset.algorithm, preset.secret)
      var decrypted = decipher.update(link, 'hex', 'utf8')
        + decipher.final('utf8')
    } catch(err) {
      callback(err, null)
      return
    }
    try {
      var decryptedLink = validator.validateLink(JSON.parse(decrypted))
      if(decryptedLink instanceof Error)
        callback(decryptedLink, null)
      else
        callback(null, decryptedLink)
    } catch(err) {
      callback(err, null)
      return
    }
  }
}
