var express = require('express');
var router = express.Router();
var tmplinker = require('tmplinker');
tmplinker.plugin(router, {
  secureDir: 'secure',
  secret: 'ewjr98456y4h5th',
  algorithm: 'rc4',
  expirationTime: '1d'
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Main Page' });
});

router.post('/createlink', function(req, res, next) {
  tmplinker.createLink(req.body.filename, (err, link) => {
    res.render('createlink', {
      title: 'Your Personal Link',
      link: link
    });
  });
});

module.exports = router;
