'use strict'

var crypto = require('crypto')
var preset = {
  secret: 'Just A Secret',
  algorithm: 'aes192',
  expirationTime: 3600,
  route: '/download'
}

exports.plugin = (router, data) => {
  if(typeof router !== 'function')
    throw new Error('Router is required')
  else if(typeof data.secureDir === 'undefined')
    throw new Error('Secure directory is required')
  else if(typeof data.expirationTime !== 'undefined' && !parseInt(data.expirationTime, 10))
    throw new Error('Expiration time should be a integer value')
  else if(typeof data.expirationTime !== 'undefined' && parseInt(data.expirationTime, 10) <= 0)
    throw new Error('Expiration time should be greater than 0')

  preset.secureDir = data.secureDir
  preset.secret = typeof data.secret === 'undefined' ? preset.secret : data.secret
  preset.algorithm = typeof data.algorithm === 'undefined' ? preset.algorithm : data.algorithm
  preset.expirationTime = typeof data.expirationTime === 'undefined' ? preset.expirationTime : data.expirationTime
  preset.route = typeof data.route === 'undefined' ? preset.route : data.route

  router.get(`${preset.route}/:link`, (req, res, next) => {
    console.log(__dirname)
    parseLink(req.params.link, (err, filename) => {
      if(err || typeof filename === 'undefined')
        res.status(400).end('Bad Request');
      else
        res.download(`${__dirname}/../../${preset.secureDir}/${filename}`)
    })
  })
}

exports.createLink = (link, callback) => {
  if(typeof link === 'undefined') {
    callback(new Error('Link is required'), null)
    return
  } else {
    let obj = {
      body: link,
      expirationTime: preset.expirationTime,
      createdAt: Date.now() / 1000 | 0
    }
    try {
      let cipher = crypto.createCipher(preset.algorithm, preset.secret)
      var crypted = cipher.update(JSON.stringify(obj), 'utf8', 'hex')
        + cipher.final('hex')
    } catch(err) {
      callback(err, null)
      return
    }
    callback(null, `${preset.route}/${crypted}`)
  }
}

var parseLink = (link, callback) => {
  if(typeof link === 'undefined') {
    callback(new Error('Link is required'), null)
  } else {
    try {
      let decipher = crypto.createDecipher(preset.algorithm, preset.secret)
      var decrypted = decipher.update(link, 'hex', 'utf8')
        + decipher.final('utf8')
    } catch(err) {
      callback(err, null)
      return
    }
    try {
      validate(JSON.parse(decrypted), (err, decryptedLink) => {
        callback(err, decryptedLink)
      })
    } catch(err) {
      callback(err, null)
      return
    }
  }
}

var validate = (link,  callback) => {
  let currentDate = Date.now() / 1000 | 0
  if(link.createdAt + link.expirationTime >= currentDate)
    callback(null, link.body)
  else
    callback(new Error('Link has expired!'), null)
}
