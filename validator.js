'use strict'

const times = {
  s: 1,        // seconds
  i: 60,       // minute
  h: 3600,     // hour
  d: 43200,    // day
  w: 302400,   // week
  m: 9072000,  // month (30 days)
  y: 108864000 // year
}

exports.validateExpirationTime = time => {
  time = time.toString()
  let pattern = /^\d+[s|i|h|d|w|m|y]?$/ig
  if(pattern.test(time)) {
    let number = parseInt(time.match(/^\d+/)[0], 10)
    let postfix = /[s|i|h|d|w|m|y]{1}$/.test(time) ? times[time.match(/[s|i|h|d|w|m|y]{1}$/)[0]] : 1
    return number * postfix
  } else {
    return new Error('Expiration time is invalid')
  }
}

exports.validateLink = link => {
  var res = link.body
  if(link.expirationTime !== 'none')
    link.createdAt + link.expirationTime < Date.now() / 1000 | 0 ? res = new Error('Link has expired!') : {}
  return res
}
